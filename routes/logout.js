var express = require("express");
var router = express.Router();

router.get("/", function(req, res, next) {
  res.clearCookie('auth').redirect("/");
});

module.exports = router;
