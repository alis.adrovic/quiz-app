var express = require('express');
var router = express.Router();

const quizController = require('../controllers/quiz-controller');

const adminAuth = require('../middleware/admin');

router.get('/', adminAuth, quizController.index);

router.get('/create', adminAuth, quizController.createQuizGet);
router.post('/create', adminAuth, quizController.createQuizPost);

router.get('/:id', quizController.getQuiz);

router.post('/:id/enroll', quizController.enrollUser);

router.get('/:id/results', adminAuth, quizController.resultsGet);

router.post('/:id/questions', adminAuth, quizController.createQuestionPost);

router.get('/:id/questions/:qid', quizController.getQustionDetails);

router.post('/:id/questions/:qid/answer', quizController.answerQuestionPost);

router.post('/:id/questions/:qid/answer/:aid', quizController.updateAnswerPost)

router.post('/:id/questions/:qid/delete', adminAuth, quizController.deleteQuestionPost);

router.get('/:id/questions/:qid/review', adminAuth, quizController.reviewQuestionGet);

router.post('/:id/questions/:qid/review/:aid', adminAuth, quizController.givePointsToAnswerPost);

router.get('/:id/user/:uid/review', quizController.reviewUserAnswersGet);

module.exports = router;
