var express = require("express");
var router = express.Router();

const dashboardController = require('../controllers/dashboard-conroller');

router.get("/", function(req, res, next) {
  res.redirect("/dashboard");
});

router.get("/dashboard", dashboardController.index);

module.exports = router;
