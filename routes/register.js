var express = require("express");
var router = express.Router();

const userController = require('../controllers/user-controller');

router.get("/", function(req, res, next) {
  res.render("auth/register");
});

router.post("/", userController.createUserPost);

module.exports = router;
