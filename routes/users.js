var express = require('express');
var router = express.Router();

const userController = require('../controllers/user-controller');
const adminAuth = require('../middleware/admin');

// Get current user
router.get('/me', userController.index);

// Get All users
router.get('/', adminAuth, userController.index);

// Get individual user
router.get('/:id', adminAuth, userController.getUser);

// Edit individual user
router.get('/:id/edit', adminAuth, userController.editUserGet);
router.post('/:id/edit', adminAuth, userController.editUserPost);


// router.get('/:id/quizes', userController.getQuizes);

// router.get('/:id/:quizes/:id', userController.getQuizById);

// GET, POST & PUT
// router.get('/:id/:quizes/:id/quiestions/:id'', userController.index);

module.exports = router;
