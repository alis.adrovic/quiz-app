const express = require('express');

const auth =require('../middleware/auth');
const router = express.Router();

const loginRouter = require('./login');
const registerRouter = require('./register');
const logoutRouter = require('./logout');
const homeRouter = require('./home');
const userRouter = require('./users');
const quizRouter = require('./quizes');
const staticRouter =require('./static');


router.use('/login', loginRouter);
router.use('/register', registerRouter);
router.use('/tutorial', staticRouter);

router.use('/logout', auth, logoutRouter);

router.use('/', auth, homeRouter);

router.use('/users', auth, userRouter);
router.use('/quizes', auth, quizRouter);

module.exports = router;
