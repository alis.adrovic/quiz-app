var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var Question = require('./question');

var QuizSchema = new Schema(
  {
    title: {type: String, required: true},
    startDate: {type: Date, required: true},
    endDate: {type: Date, required: true},
    question: [Question.schema],
    participants: [{type: Schema.Types.ObjectId, ref: 'User'}]
  }
);

QuizSchema.virtual('dispayableQuestions').get(function () {
  // Only visible after the quiz becomes "active"
  const currentTime = Date.now();
  if(this.startDate <= currentTime) {
    return this.question;
  } else {
    return [];
  }
});


QuizSchema.virtual('isActive').get(function () {
  const currentTime = Date.now();
  return this.startDate <= currentTime && currentTime <= this.endDate;
});

QuizSchema.virtual('isEnrollable').get(function () {
  const currentTime = Date.now();
  return currentTime <= this.endDate;
});

QuizSchema.virtual('isExpired').get(function () {
  const currentTime = Date.now();
  return this.endDate <= currentTime;
});

QuizSchema.virtual('numberOfParticipants').get(function () {
  return this.participant.length;
});

QuizSchema.virtual('numberOfQuestions').get(function () {
  return this.question.length;
});

//Export model
module.exports = mongoose.model('Quiz', QuizSchema);