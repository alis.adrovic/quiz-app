var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var QuestionSchema = new Schema(
  {
    title: {type: String, required: true},
    points: {type: Number, required: true},
    answer: [{type: Schema.Types.ObjectId, ref: 'Answer'}]
  }
);

//Export model
module.exports = mongoose.model('Question', QuestionSchema);