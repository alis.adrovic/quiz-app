var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AnswerSchema = new Schema(
  {
    quiz: {type: Schema.Types.ObjectId, ref: 'Quiz', required: true},
    question: {type: Schema.Types.ObjectId, ref: 'Question', required: true},
    user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    text: {type: String, required: true},
    points: {type: Number, default: 0},
    isChecked: { type: Boolean, required: true, default: false }
  }
);

//Export model
module.exports = mongoose.model('Answer', AnswerSchema);