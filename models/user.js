const Joi = require("joi");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const config = require("config");

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  birthYear: {
    type: Number,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  location: {
    city: {
      type: String,
      required: false
    },
    street: {
      type: String,
      required: false
    },
    state: {
      type: String,
      required: true
    }
  },
  enrolledQuizes: [
    {
      type: Schema.Types.ObjectId,
      ref: "Quiz"
    }
  ],
  isAdmin: { type: Boolean, required: true, default: false }
});

UserSchema.virtual("fullName").get(function() {
  return `${this.name} ${this.surname}`;
});

UserSchema.methods.generateAuthToken = function() {
  const payload = {
    _id: this._id,
    name: this.name,
    email: this.email,
    isAdmin: this.isAdmin
  };
  
  const secret = config.get("jwtSecret");

  return jwt.sign(payload, secret);
};

const User = mongoose.model("User", UserSchema);

const validateUserRegistration = user => {
  const schema = {
    name: Joi.string()
      .min(3)
      .required(),
    surname: Joi.string()
      .min(3)
      .required(),
    birthYear: Joi.number()
      .min(4)
      .required(),
    email: Joi.string()
      .min(5)
      .required()
      .email(),
    password: Joi.string().required(),
    phoneNumber: Joi.string()
      .min(9)
      .required(),
    // city: Joi.string().valid(''),
    // street: Joi.string().valid(''),
    state: Joi.string().required()
  };
  return Joi.validate(user, schema, { stripUnknown: true });
};

const validateUserLogin = user => {
  const schema = {
    email: Joi.string()
      .min(5)
      .required()
      .email(),
    password: Joi.string().required()
  };

  return Joi.validate(user, schema);
};

//Export model
exports.User = User;
exports.validateRegister = validateUserRegistration;
exports.validateLogin = validateUserLogin;
