const { User, validateRegister, validateLogin } = require("../models/user");
const _ = require("lodash");
const bcrypt = require("bcrypt");

exports.index = async (req, res) => {
  try {
    // get all the users
    const users = await User.find({});
    res.render("user/list", { users, currentUser: req.user });
  } catch (err) {
    return res.status(500).send(err);
  }
};

exports.createUserPost = async (req, res) => {
  try {
    const { error } = validateRegister(req.body);
    if (error) {
      console.log("Register user - validation error: ", error.message);
      return res.redirect("back");
    }

    let user = await User.findOne({ email: req.body.email });
    if (user) {
      const error = { message: "User already registered!" };
      return res.render("user/create", error);
    }

    user = new User({
      name: req.body.name,
      surname: req.body.surname,
      birthYear: req.body.birthYear,
      email: req.body.email,
      password: req.body.password,
      phoneNumber: req.body.phoneNumber,
      location: {
        city: req.body.city,
        street: req.body.street,
        state: req.body.state
      }
    });

    // Hash the password
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    await user.save();
    res.redirect("/users");
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      res.status(409).send(new MyError("Duplicate key", [err.message]));
    }

    res.status(500).send(err.message);
  }
};

exports.loginPost = async (req, res) => {
  try {
    const { error } = validateLogin(req.body);
    if (error) {
      console.log("ERROR: Login validation error: ", error.message);
      return res.redirect("back");
    }

    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      console.log(`ERROR: User ${req.body.email} doesn't exist`);
      return res.redirect("back");
    }

    const validPassword = await bcrypt.compare(
      req.body.password,
      user.password
    );
    if (!validPassword) {
      console.log(`ERROR: Invalid password for user ${req.body.email}`);
      return res.redirect("back");
    }

    const token = user.generateAuthToken();

    res.cookie("auth", token).redirect("/dashboard");
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      res.status(409).send(new MyError("Duplicate key", [err.message]));
    }

    res.status(500).send(err.message);
  }
};

exports.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.render("user/details", { user, currentUser: req.user });
  } catch (err) {
    return res.status(500).send(err.message);
  }
};

exports.editUserGet = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.render("user/edit", { user, currentUser: req.user });
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      res.status(409).send(new Error("Duplicate key", [err.message]));
    }
    res.status(500).send(err);
  }
};

exports.editUserPost = async (req, res) => {
  try {
    const user = {
      name: req.body.name,
      surname: req.body.surname,
      birthYear: req.body.birthYear,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      location: {
        street: req.body.street,
        city: req.body.city,
        state: req.body.state
      }
    };

    await User.findByIdAndUpdate(req.params.id, user);
    res.redirect("/users");
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      res.status(409).send(new Error("Duplicate key", [err.message]));
    }
    res.status(500).send(err);
  }
};
