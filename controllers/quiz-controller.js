const Quiz = require("../models/quiz");
const Question = require("../models/question");
const Answer = require("../models/answer");
const { User } = require("../models/user");

exports.index = async (req, res) => {
  try {
    // get all the quizes
    const quizes = await Quiz.find({});
    res.render("quiz/list", { quizes, currentUser: req.user });
  } catch (err) {
    return res.status(500).send(err);
  }
};

exports.createQuizGet = (req, res) => {
  res.render("quiz/create", { currentUser: req.user });
};

exports.createQuizPost = async (req, res) => {
  try {
    const quiz = Quiz({
      title: req.body.title,
      startDate: req.body.startDate,
      endDate: req.body.endDate
    });

    const newQuiz = await quiz.save();

    res.redirect(`/quizes/${newQuiz._id}`);
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      res.status(409).send(new MyError("Duplicate key", [err.message]));
    }

    res.status(500).send(err.message);
  }
};

exports.getQuiz = async (req, res) => {
  try {
    const quiz = await Quiz.findById(req.params.id);
    res.render("quiz/details", { quiz, currentUser: req.user });
  } catch (err) {
    return res.status(500).send(err);
  }
};

exports.createQuestionPost = async (req, res) => {
  try {
    const question = Question({
      title: req.body.title,
      points: req.body.points
    });

    console.log("Provided question: ", question);

    let quiz = await Quiz.findById(req.params.id);

    console.log("Found quiz: ", question);

    quiz.question.push(question);
    console.log("Quiz after push: ", question);
    await quiz.save();

    res.redirect(`/quizes/${req.params.id}`);
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      res.status(409).send(new MyError("Duplicate key", [err.message]));
    }

    res.status(500).send(err.message);
  }
};

exports.deleteQuestionPost = async (req, res) => {
  try {
    let quiz = await Quiz.findById(req.params.id);
    quiz.question.id(req.params.qid).remove();
    await quiz.save();

    res.redirect(`/quizes/${req.params.id}`);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.getQustionDetails = async (req, res) => {
  try {
    const quiz = await Quiz.findById(req.params.id).populate("question.answer");
    const question = quiz.question.id(req.params.qid);

    const answerOfCurrentUser = question.answer.find(
      answer => answer.user == req.user._id
    );

    res.render("question/details", { quiz, question, answerOfCurrentUser });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.answerQuestionPost = async (req, res) => {
  try {
    const quiz = await Quiz.findById(req.params.id);
    if (!quiz.isActive) {
      return res.redirect(`/quizes/${req.params.id}`);
    }

    const answer = Answer({
      quiz: req.params.id,
      question: req.params.qid,
      user: req.user._id,
      text: req.body.text
    });

    const savedAnswer = await answer.save();

    // Also add reference to question
    const question = quiz.question.id(req.params.qid);
    question.answer.push(savedAnswer._id);
    await quiz.save();

    res.redirect(`/quizes/${req.params.id}`);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.updateAnswerPost = async (req, res) => {
  try {
    const quiz = await Quiz.findById(req.params.id);
    if (!quiz.isActive) {
      return res.redirect(`/quizes/${req.params.id}`);
    }

    const answer = {
      text: req.body.text
    };

    await Answer.findByIdAndUpdate(req.params.aid, answer);

    res.redirect(`/quizes/${req.params.id}`);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.enrollUser = async (req, res) => {
  try {
    const user = await User.findById(req.user._id);
    const quiz = await Quiz.findById(req.params.id);

    //add user to quiz
    quiz.participants.push(user.id);
    quiz.save();

    //add quiz to user
    user.enrolledQuizes.push(quiz.id);
    user.save();

    res.redirect("/");
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.reviewQuestionGet = async (req, res) => {
  try {
    const quiz = await Quiz.findById(req.params.id).populate({
      path: "question.answer",
      populate: { path: "user" }
    });
    const question = quiz.question.id(req.params.qid);

    const numberOfAnswers = question.answer.length;

    //console.log(question);
    res.render("question/review", { quiz, question, numberOfAnswers });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.givePointsToAnswerPost = async (req, res) => {
  try {
    const answer = {
      points: req.body.points,
      isChecked: true
    };

    await Answer.findByIdAndUpdate(req.params.aid, answer);

    return res.redirect(
      `/quizes/${req.params.id}/questions/${req.params.qid}/review#${req.body.answerId}`
    );
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.resultsGet = async (req, res) => {
  try {
    const quiz = await Quiz.findOne({ _id: req.params.id }).populate({
      path: "participants"
      //populate: { path: "enrolledQuizes", populate: "question.answer" }
    });

    const answers = await Answer.find({ quiz: req.params.id });

    var counter = 0;
    for (participant of quiz.participants) {
      var totalPoints = 0;

      for (answer of answers) {
        if (answer.user.toString() == participant._id.toString()) {
          totalPoints += answer.points;
        }
      }

      quiz.participants[counter].totalPoints = totalPoints;
      counter++;
    }

    quiz.participants.sort(compare);

    res.render("quiz/results", { quiz });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

exports.reviewUserAnswersGet = async (req, res) => {
  try {
    const quiz = await Quiz.findOne({ _id: req.params.id });

    const userAnswers = await Answer.find({
      quiz: req.params.id,
      user: req.params.uid
    });

    let totalUserPoints = 0;
    let totalQuizPoints = 0;
    let totalQuestions = quiz.question.length;
    let totalUserAnswers = 0;

    for (answer of userAnswers) {

      const questionWithDetails = quiz.question.find(
        question => question._id.toString() == answer.question.toString()
      );
      answer.question.title = questionWithDetails.title;
      answer.question.points = questionWithDetails.points;

      //sum points
      totalUserPoints += answer.points;

      totalUserAnswers++;
    }

    for(question of quiz.question){
      totalQuizPoints += question.points;
    }

    res.render("quiz/user-answers", { quiz, userAnswers, totalUserPoints, totalQuizPoints, totalQuestions, totalUserAnswers });
  } catch (err) {
    res.status(500).send(err.message);
  }
};

// HELPERS

function compare(a, b) {
  if (a.totalPoints > b.totalPoints) return -1;
  if (a.totalPoints < b.totalPoints) return 1;
  return 0;
}
