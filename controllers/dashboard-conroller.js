const Quiz = require("../models/quiz");
const { User } = require("../models/user");

exports.index = async (req, res) => {
  try {
    // get all the quizes
    const enrollableQuizes = await Quiz.find({
      endDate: { $gte: Date.now() }
    });
    
    const curentUser = await User.findById(req.user._id).populate("enrolledQuizes");
    
    res.render("index", {
      title: "Active quizes",
      enrollableQuizes,
      enrolledQuizes: curentUser.enrolledQuizes,
      currentUser: req.user
    });
  } catch (err) {
    return res.status(500).send(err);
  }
};
