module.exports = function(req, res, next) {
    if(!req.user.isAdmin) {
        return res.render('error/403')
    }
    next();
}