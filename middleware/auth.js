const jwt = require("jsonwebtoken");
const config = require('config');

module.exports = function(req, res, next) {
    try {
        var token = req.cookies.auth;
        if(!token) return res.redirect("/login");
        
        const decodedPayload = jwt.verify(token, config.get('jwtSecret'));
        req.user = decodedPayload;
        next();
    } catch(err){
        console.log("Auth middleware error: ", err.message);
    }
}