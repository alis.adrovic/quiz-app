const createError = require("http-errors");
const express = require("express");
const Handlebars = require("handlebars");
const exphbs = require("express-handlebars");
const HandlebarsIntl = require("handlebars-intl");
const helpers = require('handlebars-helpers')();
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const config = require("config");
const mongoose = require("mongoose");
const enforce = require('express-sslify');
const helmet = require('helmet')

const indexRouter = require("./routes/index");

if (!config.get("MONGODB_URI") || !config.get("jwtSecret")) {
  console.error("FATAL ERROR: Environment variables not defined!");
  process.exit(1);
}


//Set up mongoose connection
const mongoDB = config.get("MONGODB_URI");
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

const app = express();

app.use(helmet());

const currentEnvironment = app.get('env');
if(currentEnvironment === 'production') {
  app.use(enforce.HTTPS({ trustProtoHeader: true }));
}


// view engine setup
//app.set('views', path.join(__dirname, 'views'));
app.engine(".hbs", exphbs({ defaultLayout: "layout", extname: ".hbs", helpers: helpers }));
HandlebarsIntl.registerWith(Handlebars);
app.set("view engine", "hbs");

Handlebars.registerHelper("ifEnrolled", function(
  userId,
  participants,
  options
) {
  for (participantId of participants) {
    if (participantId == userId) return options.fn(this);
  }

  return options.inverse(this);
});


app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

console.log("got here...");
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
